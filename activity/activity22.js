/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
function addRegisteredUsers(name){
	let isExist = false;
	for(let x=0; x<=registeredUsers.length-1; x++){
		if(name == registeredUsers[x]){
			isExist = true;
		}
	}
	if(isExist == false){
		registeredUsers[registeredUsers.length] = name;
		alert("Thank you for registering!");
	}else{
		alert("Registration failed. Username already exists!");
	}
}
console.log("register (\"Conan O' Brien\")");
console.log(undefined);
addRegisteredUsers("Conan O' Brien");
console.log(registeredUsers);




console.log("register (\"Akiko Yukihime\")");
console.log(undefined);
addRegisteredUsers("Akiko Yukihime");
console.log(registeredUsers);
console.log("registeredUsers");
console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/


function addFriendList(username){
	let isExist = false;
	for(let x=0; x<=registeredUsers.length-1; x++){
		if(username == registeredUsers[x]){
			isExist = true;
		}
	}
	if(isExist == true){
		friendsList[friendsList.length] = username;
		alert("Thank you for registering!");
	}else{
		alert("User not found.");
	}
}
console.log("addFriend(\"Akiko Yukihime\")");
console.log(undefined);
console.log("friendsList");
addFriendList("Akiko Yukihime");
console.log(friendsList);



console.log("addFriend(\"Conan O' Brien\")");
console.log(undefined);
console.log("friendsList");
addFriendList("Conan O' Brien");
console.log(friendsList);




console.log("addFriend(\"James Gunn\")");
console.log(undefined);
console.log("friendsList");
addFriendList("James Gunn");
console.log(friendsList);


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/



if(friendsList.length == 0){
	console.log("You currently have 0 friends. Add one first.")
}else{
	console.log(friendsList.join("\n"));
	}
console.log(undefined);




/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
function displayNumberofFriends(){
	if (friendsList.length == 0){
		alert("You currently have 0 friends. Add one first.");
	}else{
		alert("You currently have "+friendsList.length+" friends.")
	}
}
displayNumberofFriends();
console.log("displayNumberofFriends()")

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
function deleteLastFriendList(){
	friendsList.splice(friendsList.length-1, 1);
	console.log(friendsList);
}
deleteLastFriendList();
// console.log("this is last"+registeredUsers[registeredUsers.length-1])

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
function spliceLastFriendList(){
	friendsList.splice(0, 1);
	console.log(friendsList);
}
spliceLastFriendList();