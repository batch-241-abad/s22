/*
	ARRAY MUTATOR
	for array methods, we have the mutato and non mutator methods

	MUTATOR METHODS
	-functions that MUTATE or change an array after they're created
	-these methods manipulate the original array. Performing tasks such as adding and removing elements 
	-push, pop, unshift, shift, splice, sort, reverse
	-
*/
console.log("%cMutator Methods", "font-weight: bold");
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits);
console.log("")

/*
	PUSH
	"adds" in the "end" of the array AND returnds the array's length
	-[0,0,"X"];

	SYNTAX:
		arrayName.push();
*/
console.log("%cPUSH METHOD", "background: #89cff0")
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method");
console.log(fruits);

// Push multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method: (Multpile values)");
console.log(fruits);
console.log("")


/*
	POP
	REMOVES the last element in an array AND returnds the removed element
	- [0,0,"X"]

	SYNTAX:
		arrayName.pop();
*/

console.log("%cPOP METHOD:", "background: #89cff0");
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);
console.log("")

/*
	UNSHIFT
	ADDS at the START of an array
	-["x",0,0];

	SYNTAX:
		arrayName.unshift("elementA");
		arrayName.unshift("elementA", "elementB");
*/

console.log("%cUNSHIFT METHOD:", "background: #89cff0");
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:");
console.log(fruits);
console.log("");


/*
	SHIFT

	removes element at the START of an array AND returns the removed element

	arrayName.shift();
*/
console.log("%cSHIFT METHOD:", "background: #89cff0");
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:")
console.log(fruits);
console.log("");


/*
	SPLICE

	simutaneously removes element from a specified number and adds elements

	SYNTAX:
		arrayName.splice(whereToStartDeleting, numberOfItemsToBeDeleted, elementsToBeAdded);
*/

console.log("%cSPLICE METHOD:", "background: #89cff0");
fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method: ")
console.log(fruits);
console.log("");

/*
	SORT

	rearranges the array elements in an Alphanumberic order

	SYNTAX:
		arrayName.sort()
*/

console.log("%cSORT METHOD:", "background: #89cff0");
fruits.sort();
console.log("Mutated array from sort method: ")
console.log(fruits);
console.log("");

/*
	reverse()

	reverse the order of array elements

	SYNTAX:
		arrayName.reverse()
*/

console.log("%cREVERSE METHOD:", "background: #89cff0");
fruits.reverse();
console.log("Mutated array from reverse method: ")
console.log(fruits);
console.log("");
console.warn("END OF MUTATOR METHOD");
console.log("");
