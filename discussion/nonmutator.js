/*
	NON MUTATOR METHODS

	- fucntions that do not modify or change an array after they have been created
	-these methods do not manipulate the original array
	-these methods perform tasks such as returning or getting elements from an array, combining arrays, and printing output
	-indexOf(), lastIndexOf(), slice(), toString(), concat(), join()
*/
console.log("%cNon-Mutator Methods", "font-weight: bold");
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]
console.log(countries);
console.log("");

/*
	indexOf

	-returns the index number of the FIRST MATCHING ELEMENT found in an array
	-if no match has been found, the result will be -1
	-the research process will be done from the first element proceeding to the last elements

	SYNTAX:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/
console.log("%cindexOf Methods", "background: #89cff0");
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: "+firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: "+invalidCountry);
console.log("")

/*
	lastIndexOf()

	-returnds the index number of the LAST MATCHING ITEM found in the array
	-the research process will be done from the last element proceeding to the first element.

	SYNTAX:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/
console.log("%clastIndexOf Methods", "background: #89cff0");

// getting the index number starting from the lsat element
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: "+lastIndex);

// getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf("PH", 6);
console.log("Result of lastIndexOf method: "+lastIndexStart);
let lastIndexStarter = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: "+lastIndexStarter);
console.log("");

/*
	SLICE
	-slices elements from an array AND returns a new array

	SYNTAX:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex)
*/
console.log("%cSLICE Methods", "background: #89cff0");
let sliceArrayA = countries.slice(2);
console.log("Result of slice method: ");
console.log(sliceArrayA);

// slicing off elements from a specified index to another index
let sliceArrayB = countries.slice(2, 4);
console.log("Result of slice method: ");
console.log(sliceArrayB);

// slicing off elements starting from the last element of an array
let sliceArrayC = countries.slice(-5, -2);
let sliceArrayD = countries.slice(-3);
let sliceArrayE = countries.slice(-5);
console
console.log("Result of slice method: ");
console.log(sliceArrayC);
console.log(sliceArrayD);
console.log(sliceArrayE);
console.log("");

/*
	toString();

	returns an array as a string seperated by commas
		arrayName.toString()
*/
console.log("%ctoString() METHOD:", "background: #89cff0");
let stringArray = countries.toString();
console.log("Result of toString method");
console.log(stringArray)
console.log("");

/*
	CONCAT

	combies two arrays and returns the combined result
		arrayA.concat(arrayB);
		arrayA.concat(elementA, elementB);
*/
console.log("%cconcat() METHOD:", "background: #89cff0");
let tasksArrayA = ["drink HTML", "eat javascript"];
let tasksArrayB = ["inhale CSS", "breath sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result of concat method");
console.log(tasks);

// combining multiple arrays
console.log("Result from concat method: ");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// combining arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat method");
console.log(combinedTasks);
console.log("");

/*
	JOIN

	returns an array as a string seperated by specified seperator string

	arrayName.join("separatorString");
*/

console.log("%cjoin() METHOD:", "background: #89cff0");

let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(" - "));
console.log("");

console.warn("END OF NON MUTATOR METHODS");


console.log("");
console.log("%cITERATION METHOD:", "font-weight: bold");

/*
	ITERATION METHODS

	-iteration methods are loops designed to perform repetitive tasks on arrays
	-useful for manipulating array data resulting in complex tasks
*/

/*
	forEach()

	-similar to the for loop that iterates on ech array element
	-variable names for arrays are usually written in the plural form of the data stored in an array
	-it is a common practice to use the singular form of the array content for parameter names used in array loops
	-array iteration methods normally work with a function supplied as an argument

		arrayName.forEach(function(individualElement){statement})
	


*/

allTasks.forEach(function(task){
	console.log(task)
	});
console.log("");

let students = ["Jeru", "Ann", "Glyn123", "Jake"];
students.forEach(function(student){console.log("Hi, "+student+" !")});
console.log("");

// using foreach with conditional statements
console.log("%cFiltered forEach:", "text-decoration: underline");

let filteredTasks = [];

allTasks.forEach(function(task) {
	if(task.length > 10){
		console.log(task);
		filteredTasks.push(task);
	}
});
console.log("Result of filtered tasks:");
console.log(filteredTasks);

students.forEach(function(student){
	if(student.length <= 4){
		console.log("Sa kanan uupo si "+student);
	}else{
		console.log("sa kaliwa uupo is "+student);
	}
})

console.log("");

/*
	map()

	-it iterates of each element AND returns new array with different values depending on the result of the function's operation
	-this is useful for performing tasks where mutating or changing the elements are required
	-unlike the forEach method, the map method requires the use of a RETURN statement in order to create another array with the performed operation

	let/const resultArray = arrayName.map(function(individualElement));
*/

let numbers = [1, 2, 3, 4, 5];
console.log("Before the map method: "+numbers);

let numberMap = numbers.map(function(number){
	return number*number;
});

console.log("Result of map method: ");
console.log(numberMap);
console.log("");


/*
	every()

	-checks if all elements in an array meet the given condition
	-this is useful for validating data stored in arrays specially when dealing with large amounts of data.
	-returns the true value if all elements meet the condition and false if otherwise

		let/const resultArray = arrayName.every(function(element){
			return expression/condition
		});
*/

let allValid = numbers.every(function(number) {
	return (number < 3);
});
console.log("Result of every method:");
console.log(allValid);
console.log("");


/*
	some()

	-checks if atleast one element in the array meets the given condition
	-returns a true value if atleast one element meets the condition and false if otherwise

		let/const resultArray = arrayName.some(function(element){
			return expression/condition
		});
*/
console.log("%cevery() Method:", "background: #89cff0");

let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log("Result of some method:");
console.log(someValid);
// combining the returned result from the every or some method may be used in other statements to perform consecutive
if(someValid){
	console.log("some numbers in the array are greater than 2");
}
console.log("")



/*
	filter()

	-returns new array that contains element which meets the given condition
	-returns an empty array if no elements are found
	-useful for filtering array elements with a given condition and shorten the syntax compared to using other array iteration methods.

	let / const resultArray = arrayName.filter(function(element){
		return expression/condition
	})
*/
console.log("%cfilter() Method:", "background: #89cff0");
let filterValid = numbers.filter(function(number){
	return (number < 3)
});
console.log("Result of the filter method:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number = 0);
});
console.log(nothingFound);

// filtering using forEach
let filteredNumber = [];

numbers.forEach(function(number){
	console.log(number);
	if(number < 3){
		filteredNumber.push(number);
	}
});
console.log("Result of filter method:");
console.log(filteredNumber);
console.log("");

/*
	includes() method

	-methods can be CHAINED by using them one after another
	-the reuslt of the first method is used on the second method until all chained method have been solved.
*/



console.log("%cincludes() Method:", "background: #89cff0");

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
});
console.log(filteredProducts);
console.log("");

/*
	reduce()

	-evaluates statements from left to right and returns or reduces the array into a single value

	
	let/const resultArr = arrayName.reduce(fucntion(accumulator, currentValue){
		return expression/condition
	});

	
	-The ACCUMULATOR parameter in the function stores the result for every iteration of the loop
	-the currentValue is the current/next element in the array that is evaluated in each iteration of the loop
	-how the reduce method works:
		1. The first or result element in the array is stores in the ACCUMULATOR parameters
		2. the second or next element in the array is stores in the "currentValue"
		3. An operation is performed on the two elements
		4. The loop repeats step 1 to 3 until all elements have been worked on.
*/




console.log("%creduce() Method:", "background: #89cff0");
let iteration = 0;

let reducedArray = numbers.reduce(function(x,y){
	console.warn("current iteration:" + ++iteration);
	console.log("accumulator: "+ x);
	console.log("curentValue: "+y);

	// the operation to reduce to the array into a single value
	return x+y;
});

console.log("result of reduce method: "+reducedArray);


// reducing string arrays
let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function(x,y){
	return x+" "+y;
});
console.log("Result of reduce method: "+reducedJoin);